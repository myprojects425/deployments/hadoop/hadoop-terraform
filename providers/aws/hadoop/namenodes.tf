# NAMENODE TYPES: active, standby and observers

########################################
# Definition of namenodes nodes (primary namenode, standby namenodes and observer namenodes)
########################################

resource "aws_instance" "primary_namenode" {
  ami             = data.aws_ami.hadoop_ami.image_id
  instance_type   = var.instance_type__namenode
  security_groups = [aws_security_group.allow_all.id] # CREATE CUSTOM RULES
  # allocate the same amount of nodes in the created subnets (1 subnet by availability zone)
  subnet_id       = module.vpc.public_subnets[0]
  key_name        = var.key_pair
  associate_public_ip_address = true

  tags = {
    Name = "primary_nodename"
    Role = "NodeName"
    NameNodeType = "Primary"
    Zone = module.vpc.azs[0]
  }
}


resource "aws_instance" "standby_namenode" {
  count           = var.number_standby_namenode_nodes
  ami             = data.aws_ami.hadoop_ami.image_id
  instance_type   = var.instance_type__namenode
  security_groups = [aws_security_group.allow_all.id] # CREATE CUSTOM RULES
  # allocate the same amount of nodes in the created subnets (1 subnet by availability zone)
  subnet_id       = module.vpc.public_subnets[count.index % length(module.vpc.public_subnets)]
  key_name        = var.key_pair
  associate_public_ip_address = true

  tags = {
    Name = "standby_nodename${count.index}"
    Role = "NodeName"
    NameNodeType = "Standby"
    Zone = module.vpc.azs[count.index % length(module.vpc.private_subnets)]
  }
}


# resource "aws_instance" "observer_namenode" {
#   count           = var.number_observer_namenode_nodes
#   ami             = data.aws_ami.hadoop_ami.image_id
#   instance_type   = var.instance_type__namenode
#   security_groups = [aws_security_group.allow_all.id] # CREATE CUSTOM RULES
#   # allocate the same amount of nodes in the created subnets (1 subnet by availability zone)
#   subnet_id       = module.vpc.public_subnets[count.index % length(module.vpc.public_subnets)]
#   key_name        = var.key_pair
#   associate_public_ip_address = true

#   tags = {
#     Name = "observer_nodename${count.index}"
#     Role = "NodeName"
#     NameNodeType = "Observer"
#     Zone = module.vpc.azs[count.index % length(module.vpc.private_subnets)]
#   }
# }


########################################
# inventory for namenode nodes
#
# NOTE: This inventory is created for post-operations of hadoop cluster resources
########################################

resource "ansible_group" "primary_namenode" {
  name     = "primary_namenode"
  children = []
}

resource "ansible_group" "standby_namenodes" {
  name     = "standby_namenodes"
  children = []
}

resource "ansible_group" "observer_namenodes" {
  name     = "observer_namenodes"
  children = []
}


# add host to ansible inventory (mainly for post operation of hadoop cluster)
resource "ansible_host" "primary_namenode" {
  name            = aws_route53_record.primary_namenode.name
  groups          = ["primary_namenode"]

  variables  = {
    ansible_connection = "ssh"
    ansible_user  = "ubuntu"
    ansible_ssh_private_key_file = "~/.aws/ssh/bell-virg.pem"
  }
}

resource "ansible_host" "standby_namenode" {
  count           = var.number_standby_namenode_nodes
  name            = aws_route53_record.standby_namenode[count.index].name
  groups          = ["standby_namenodes"]

  variables  = {
    ansible_connection = "ssh"
    ansible_user  = "ubuntu"
    ansible_ssh_private_key_file = "~/.aws/ssh/bell-virg.pem"
  }
}

# resource "ansible_host" "observer_namenodes" {
#   count           = var.number_observer_namenode_nodes
#   name            = aws_route53_record.observer_namenode[count.index].name
#   groups          = ["observer_namenodes"]

#   variables  = {
#     ansible_connection = "ssh"
#     ansible_user  = "ubuntu"
#     ansible_ssh_private_key_file = "~/.aws/ssh/bell-virg.pem"
#   }
# }


resource "ansible_group" "namenodes" {
  name     = "namenodes"
  children = ["primary_namenode", "standby_namenodes", "observer_namenodes"]
}
