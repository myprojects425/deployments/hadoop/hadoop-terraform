# NodeManager == DataNode == Worker Nodes

########################################
# Definition of worker nodes
########################################

resource "aws_instance" "worker" {
  count           = var.number_worker_nodes
  ami             = data.aws_ami.hadoop_ami.image_id
  instance_type   = var.instance_type__worker
  #security_groups = [aws_security_group.allow_datanode.id, aws_security_group.allow_nodemanager.id]
  security_groups = [aws_security_group.allow_all.id]
  # allocate the same amount of nodes in the created subnets (1 subnet by availability zone)
  subnet_id       = module.vpc.private_subnets[count.index % length(module.vpc.private_subnets)]
  key_name        = var.key_pair

  tags = {
    Name = "worker${count.index}"
    Role = "Worker"
    Zone = module.vpc.azs[count.index % length(module.vpc.private_subnets)]
  }
}

resource "aws_ebs_volume" "worker_data_volume" {
  count             = var.number_worker_nodes
  availability_zone = module.vpc.azs[count.index % length(module.vpc.private_subnets)]
  size              = var.worker_data_volume_size

  tags = {
    Name = "WorkerDataVolume${count.index}"
  }
}

resource "aws_volume_attachment" "worker_data_volume_attachment" {
  count       = var.number_worker_nodes
  device_name = "/dev/sdz"
  volume_id   = aws_ebs_volume.worker_data_volume[count.index].id
  instance_id = aws_instance.worker[count.index].id
}


########################################
# inventory for worker nodes (datanodes and nodemanagers)
#
# NOTE: This inventory is created for post-operations of hadoop cluster resources
########################################

resource "ansible_group" "workers" {
  name     = "workers"
  children = []
}


# add host to ansible inventory (mainly for post operation of hadoop cluster)
resource "ansible_host" "worker" {
  count           = var.number_worker_nodes
  name            = aws_route53_record.worker[count.index].name
  groups          = ["workers"]

  variables  = {
    ansible_connection = "ssh"
    ansible_user  = "ubuntu"
    ansible_ssh_private_key_file = "~/.aws/ssh/bell-virg.pem"
  }
}

resource "ansible_group" "datanodes" {
  name     = "datanodes"
  children = ["workers"]
}

resource "ansible_group" "nodemanagers" {
  name     = "nodemanagers"
  children = ["workers"]
}
