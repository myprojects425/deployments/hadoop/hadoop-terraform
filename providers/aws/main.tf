terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}


provider "aws" {
  region = var.aws_region
}


# resource "random_string" "random" {
#   length           = 6
#   special          = false
# }


# locals {
#   public_dns_zone = "${random_string.random.result}_${var.dns_zone_name}"
#   private_dns_zone = "${random_string.random.result}_${var.dns_zone_name}"
# }


module "hadoop" {
  source = "./hadoop"

  #public_dns_zone_name = local.public_dns_zone # only for testing purpose (replace it with $var.zone_name)
  #private_dns_zone_name = local.private_dns_zone # only for testing purpose (replace it with $var.zone_name)
  #ansible_verbose_level = var.ansible_verbose_level
  key_pair = var.key_pair
  number_worker_nodes = var.number_worker_nodes
}
